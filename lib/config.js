"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MVC_VIEW_ELEMENT = exports.MVC_INPUT_CHANGED = exports.MVC_PROPERTY_CHANGED = exports.MVC_MODEL_MODIFIED = exports.MVC_CHECK_GROUP = exports.MVC_RADIO_GROUP = exports.MVC_CONTROLS = exports.MVC_OBSERVES = void 0;
var APP_PREFIX = 'mvc';
var MVC_OBSERVES = "".concat(APP_PREFIX, "-observes");
exports.MVC_OBSERVES = MVC_OBSERVES;
var MVC_CONTROLS = "".concat(APP_PREFIX, "-controls");
exports.MVC_CONTROLS = MVC_CONTROLS;
var MVC_RADIO_GROUP = "".concat(APP_PREFIX, "-radio-group");
exports.MVC_RADIO_GROUP = MVC_RADIO_GROUP;
var MVC_CHECK_GROUP = "".concat(APP_PREFIX, "-check-group");
exports.MVC_CHECK_GROUP = MVC_CHECK_GROUP;
var MVC_MODEL_MODIFIED = "".concat(APP_PREFIX, "-model-modified");
exports.MVC_MODEL_MODIFIED = MVC_MODEL_MODIFIED;
var MVC_PROPERTY_CHANGED = "".concat(APP_PREFIX, "-property-changed");
exports.MVC_PROPERTY_CHANGED = MVC_PROPERTY_CHANGED;
var MVC_INPUT_CHANGED = "".concat(APP_PREFIX, "-input-changed");
exports.MVC_INPUT_CHANGED = MVC_INPUT_CHANGED;
var MVC_VIEW_ELEMENT = "".concat(APP_PREFIX, "-view");
exports.MVC_VIEW_ELEMENT = MVC_VIEW_ELEMENT;
//# sourceMappingURL=config.js.map