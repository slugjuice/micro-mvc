const APP_PREFIX = 'mvc';

export const MVC_OBSERVES = `${APP_PREFIX}-observes`;
export const MVC_CONTROLS = `${APP_PREFIX}-controls`;
export const MVC_RADIO_GROUP = `${APP_PREFIX}-radio-group`;
export const MVC_CHECK_GROUP = `${APP_PREFIX}-check-group`;

export const MVC_MODEL_MODIFIED = `${APP_PREFIX}-model-modified`;
export const MVC_PROPERTY_CHANGED = `${APP_PREFIX}-property-changed`;
export const MVC_INPUT_CHANGED = `${APP_PREFIX}-input-changed`;


export const MVC_VIEW_ELEMENT = `${APP_PREFIX}-view`;
